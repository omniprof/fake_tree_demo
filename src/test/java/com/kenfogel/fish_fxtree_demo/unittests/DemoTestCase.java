package com.kenfogel.fish_fxtree_demo.unittests;

import static org.junit.Assert.assertEquals;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.Reader;
import java.io.StringReader;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.LinkedList;
import java.util.List;
import java.util.Scanner;

import org.junit.Before;
import org.junit.Test;

import com.kenfogel.fake_tree_demo.beans.FishData;
import com.kenfogel.fake_tree_demo.persistence.FishDAO;
import com.kenfogel.fake_tree_demo.persistence.FishDAOImpl;
import javafx.collections.ObservableList;

public class DemoTestCase {

    /**
     * This test should prove that there are 200 records in the database
     *
     * @throws SQLException
     */
    @Test
    public void testFindAll() throws SQLException {
        FishDAO fd = new FishDAOImpl();
        ObservableList<FishData> lfd = fd.findTableAll();
        assertEquals("testFindAll: ", 3, lfd.size());
    }
}
