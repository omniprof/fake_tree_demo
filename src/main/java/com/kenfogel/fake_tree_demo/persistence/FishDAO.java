package com.kenfogel.fake_tree_demo.persistence;

import com.kenfogel.fake_tree_demo.beans.FishData;

import java.sql.SQLException;
import java.util.ArrayList;

import javafx.collections.ObservableList;

/**
 * Interface for CRUD methods
 *
 * @author Ken
 */
public interface FishDAO {

    public ObservableList<FishData> findTableAll() throws SQLException;
}
