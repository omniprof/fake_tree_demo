/**
 * Returns a list of three beans for UI testing
 * @author Ken Fogel
 * @version 1.7
 */
package com.kenfogel.fake_tree_demo.persistence;

import java.sql.SQLException;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

import com.kenfogel.fake_tree_demo.beans.FishData;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * This class implements the FishDAO interface
 *
 * Exceptions are possible whenever a JDBC object is used. When these exceptions
 * occur it should result in some message appearing to the user and possibly
 * some corrective action. To simplify this, all exceptions are thrown and not
 * caught here. The methods that you write to call any of these methods must
 * either use a try/catch or continue throwing the exception.
 *
 * Updated by creating a new method createFishData that used the resultSet to
 * create an object. Originally this code was repeated three times.
 *
 * @author Ken
 * @version 1.6
 *
 */
public class FishDAOImpl implements FishDAO {

    private final static Logger LOG = LoggerFactory.getLogger(FishDAOImpl.class);

    /**
     * Retrieve all the records for the given table and returns the data as an
     * arraylist of FishData objects
     *
     * @return The arraylist of FishData objects
     * @throws java.sql.SQLException
     */
    @Override
    public ObservableList<FishData> findTableAll() throws SQLException {

        ObservableList<FishData> fishDataList = FXCollections
                .observableArrayList();

        FishData fishData = new FishData(1,"Moose Fish","","","","","","","","","");
        fishDataList.add(fishData);
        fishData = new FishData(2,"Cat Fish","","","","","","","","","");
        fishDataList.add(fishData);
        fishData = new FishData(3,"Dog Fish","","","","","","","","","");
        fishDataList.add(fishData);
        return fishDataList;
    }
}
