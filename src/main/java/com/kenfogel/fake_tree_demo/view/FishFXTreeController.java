package com.kenfogel.fake_tree_demo.view;

import java.sql.SQLException;

import com.kenfogel.fake_tree_demo.beans.FishData;
import com.kenfogel.fake_tree_demo.persistence.FishDAO;

import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.control.TreeItem;
import javafx.scene.control.TreeView;
import javafx.scene.control.TreeCell;
import javafx.scene.image.ImageView;
import javafx.scene.layout.AnchorPane;

public class FishFXTreeController {

    private FishDAO fishDAO;

    @FXML
    private AnchorPane fishFXTreeLayout;

    @FXML
    private TreeView<FishData> fishFXTreeView;

    /**
     * Initializes the controller class. This method is automatically called
     * after the fxml file has been loaded.
     */
    @FXML
    private void initialize() {

        // We need a root node for the tree and it must be the same type as all nodes
        FishData rootFish = new FishData();
        // The tree will display common name so we set this for the root
        rootFish.setCommonName("Fishies");
        fishFXTreeView.setRoot(new TreeItem<FishData>(rootFish));

        // This cell factory is used to choose which field in the FihDta object is used for the node name
        fishFXTreeView.setCellFactory((e) -> new TreeCell<FishData>() {
            @Override
            protected void updateItem(FishData item, boolean empty) {
                super.updateItem(item, empty);
                if (item != null) {
                    setText(item.getCommonName());
                    setGraphic(getTreeItem().getGraphic());
                } else {
                    setText("");
                    setGraphic(null);
                }
            }
        });
    }

    /**
     * Sets a reference to the FishDAO object that retrieves data from the
     * database. With this inform
     *
     * @param fishDAO
     * @throws SQLException
     */
    public void setFishDAO(FishDAO fishDAO) {
        this.fishDAO = fishDAO;
    }

    /**
     * Build the tree from the database
     *
     * @throws SQLException
     */
    public void displayTree() throws SQLException {
        // Retreive the list of fish
        ObservableList<FishData> fishies = fishDAO.findTableAll();

        // Build an item for each fish and add it to the root
        if (fishies != null) {
            for (FishData fd : fishies) {
                TreeItem<FishData> item = new TreeItem<>(fd);
                item.setGraphic(new ImageView(getClass().getResource("/images/fish.png").toExternalForm()));
                fishFXTreeView.getRoot().getChildren().add(item);
            }
        }

        // Open the tree
        fishFXTreeView.getRoot().setExpanded(true);

        // Listen for selection changes and show the fishData details when changed.
        fishFXTreeView
                .getSelectionModel()
                .selectedItemProperty()
                .addListener(
                        (observable, oldValue, newValue) -> showFishDetails(newValue));
    }

    /**
     * To be able to test the selection handler for the tree, this method
     * displays the FishData object that corresponds to the selected node.
     *
     * @param fishData
     */
    private void showFishDetails(TreeItem<FishData> fishData) {
        System.out.println(fishData.getValue());
    }
}
