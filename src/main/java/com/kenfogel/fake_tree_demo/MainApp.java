package com.kenfogel.fake_tree_demo;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.kenfogel.fake_tree_demo.persistence.FishDAO;
import com.kenfogel.fake_tree_demo.persistence.FishDAOImpl;
import com.kenfogel.fake_tree_demo.view.FishFXTreeController;
import java.io.IOException;
import java.sql.SQLException;

public class MainApp extends Application {

    private final static Logger LOG = LoggerFactory.getLogger(MainApp.class);

    private Stage primaryStage;
    private AnchorPane fishFXTreeLayout;
    private final FishDAO fishDAO;

    public MainApp() {
        super();
        fishDAO = new FishDAOImpl();
    }

    @Override
    public void start(Stage primaryStage) throws Exception {
        this.primaryStage = primaryStage;
        this.primaryStage.setTitle("Fish Tree");

        // Set the application icon using getResourceAsStream.
        this.primaryStage.getIcons().add(
                new Image(MainApp.class
                        .getResourceAsStream("/images/bluefish_icon.png")));

        initRootLayout();
        primaryStage.show();
    }

    /**
     * Load the layout and controller.
     */
    public void initRootLayout() {
        try {
            // Load root layout from fxml file.
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(MainApp.class
                    .getResource("/fxml/FishFXTreeLayout.fxml"));
            fishFXTreeLayout = (AnchorPane) loader.load();

            // Show the scene containing the root layout.
            Scene scene = new Scene(fishFXTreeLayout);
            primaryStage.setScene(scene);

            FishFXTreeController controller = loader.getController();
            controller.setFishDAO(fishDAO);
            controller.displayTree();
        } catch (IOException | SQLException e) {
            LOG.error("Error display table", e);
        }
    }

    /**
     * Where it all begins
     *
     * @param args
     */
    public static void main(String[] args) {
        launch(args);
        System.exit(0);
    }
}
